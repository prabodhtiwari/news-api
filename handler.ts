/**
 * handler for news-api
 */

"use strict";

import redis from "./config/redis/client";

import {
    ReplyNoContinue,
    Request,
} from "hapi";

import {
    ERROR_STATUS,
    PARAMETER_MISSING_CODE,
    PARAMETER_MISSING_MESSAGE,
    SOMETHING_WENT_WRONG_CODE,
    SOMETHING_WENT_WRONG_MESSAGE,
    SUCCESS_CODE,
    SUCCESS_STATUS,
} from "./config/constants";

import {
    getNews,
} from "./utils";

const handler = {
    news(request: Request, reply: ReplyNoContinue) {
        try {
            const country = request.query.country;
            const category = request.query.category;
            const keyword = request.query.keyword;

            if (!country || !category) {
                reply({
                    code: PARAMETER_MISSING_CODE,
                    message: PARAMETER_MISSING_MESSAGE,
                    status: ERROR_STATUS,
                }).code(400);
                return;
            }

            getNews(country, category, keyword)
                .then(reply);

        } catch (err) {
            reply({
                code: SOMETHING_WENT_WRONG_CODE,
                message: SOMETHING_WENT_WRONG_MESSAGE,
                status: ERROR_STATUS,
            }).code(400);
        }
    },
};

export default handler;
