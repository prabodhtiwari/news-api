FROM       mhart/alpine-node:6

# Copy package.json
COPY        package.json /tmp
RUN         cd /tmp && npm install

# Create app directory
RUN         mkdir -p /usr/src/app
RUN         cp -R /tmp/node_modules /usr/src/app/node_modules

# Change the working directory to the app directory
WORKDIR     /usr/src/app

# Bundle app source
COPY        . /usr/src/app

# Expose port 3000 ON ALL INTERFACES
EXPOSE      3000

# Start the app
CMD         [ "npm", "start" ]
