/**
 * test for news-api
 */

"use strict";

import * as chai from "chai";

import * as chaiHttp from "chai-http";
import server from "./index";

import redis from "./config/redis/client";
import {
    getRedisKey,
} from "./utils";

import {
    ERROR_STATUS,
    HOST,
    PARAMETER_MISSING_CODE,
    PARAMETER_MISSING_MESSAGE,
    PORT,
    SUCCESS_CODE,
    SUCCESS_STATUS,

} from "./config/constants";

const should = chai.should();

chai.use(chaiHttp);

server.register([{
    register: require("inject-then"),
}]);

const testData = {
    category: "sports",
    country: "in",
    keyword: "2018",
};

describe("News", () => {
    redis.del(getRedisKey(testData.country, testData.category));
    describe("/GET news", () => {
        it(`it should GET news from API for \n country = ${testData.country} \n category = ${testData.category} \n keyword = ${testData.keyword} \n`, (done) => {
            chai.request(`http://${HOST}:${PORT}`)
                .get(`/news?country=${testData.country}&category=${testData.category}&keyword=${testData.keyword}`)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a("object");
                    res.body.should.have.property("status").eql(SUCCESS_STATUS);
                    res.body.should.have.property("code").eql(SUCCESS_CODE);
                    res.body.should.have.property("articles").be.a("array");
                    res.body.should.have.property("fromCache").eql(false);
                    done();
                });
        });

        it(`it should GET news from cache for \n country = ${testData.country} \n category = ${testData.category} \n keyword = ${testData.keyword} \n`, (done) => {
            chai.request(`http://${HOST}:${PORT}`)
                .get(`/news?country=${testData.country}&category=${testData.category}&keyword=${testData.keyword}`)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a("object");
                    res.body.should.have.property("status").eql(SUCCESS_STATUS);
                    res.body.should.have.property("code").eql(SUCCESS_CODE);
                    res.body.should.have.property("articles").be.a("array");
                    res.body.should.have.property("fromCache").eql(true);
                    done();
                });
        });

        it(`it should GET news for \n country = ${testData.country} \n category = ${testData.category} \n`, (done) => {
            chai.request(`http://${HOST}:${PORT}`)
                .get(`/news?country=${testData.country}&category=${testData.category}`)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a("object");
                    res.body.should.have.property("status").eql(SUCCESS_STATUS);
                    res.body.should.have.property("code").eql(SUCCESS_CODE);
                    res.body.should.have.property("articles").be.a("array");
                    res.body.should.have.property("fromCache").be.a("boolean");
                    done();
                });
        });

        it(`it should not GET news for \n country = ${testData.country} \n keyword = ${testData.keyword} \n`, (done) => {
            chai.request(`http://${HOST}:${PORT}`)
                .get(`/news?country=${testData.country}&keyword=${testData.keyword}`)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a("object");
                    res.body.should.have.property("status").eql(ERROR_STATUS);
                    res.body.should.have.property("code").eql(PARAMETER_MISSING_CODE);
                    res.body.should.have.property("message").eql(PARAMETER_MISSING_MESSAGE);
                    done();
                });
        });

        it(`it should not GET news for \n category = ${testData.category} \n keyword = ${testData.keyword} \n`, (done) => {
            chai.request(`http://${HOST}:${PORT}`)
                .get(`/news?category=${testData.category}&keyword=${testData.keyword}`)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a("object");
                    res.body.should.have.property("status").eql(ERROR_STATUS);
                    res.body.should.have.property("code").eql(PARAMETER_MISSING_CODE);
                    res.body.should.have.property("message").eql(PARAMETER_MISSING_MESSAGE);
                    done();
                });
        });

        it(`it should not GET news for \n keyword = ${testData.keyword} \n`, (done) => {
            chai.request(`http://${HOST}:${PORT}`)
                .get(`/news?keyword=${testData.keyword}`)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a("object");
                    res.body.should.have.property("status").eql(ERROR_STATUS);
                    res.body.should.have.property("code").eql(PARAMETER_MISSING_CODE);
                    res.body.should.have.property("message").eql(PARAMETER_MISSING_MESSAGE);
                    done();
                });
        });

    });
});
