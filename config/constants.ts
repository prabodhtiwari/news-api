/**
 * constants for news-api
 */

"use strict";

export const HOST = "localhost";
export const PORT = "3000";

export const REDIS_HOST = process.env.REDIS_HOST;
export const REDIS_PORT = process.env.REDIS_PORT;

export const CACHE_EXPIRE_TIME = 600;

export const NEWS_API_URI = "https://newsapi.org/v2/top-headlines";
export const NEWS_API_KEY = "eadb6da4bb5847a8b5f5b8a633e53ab9";

export const PARAMETER_MISSING_MESSAGE =
    "Required parameters are missing. Please set following parameters and try again: country, category.";
export const ERROR_STATUS = "error";
export const PARAMETER_MISSING_CODE = "parameterMissing";

export const SOMETHING_WENT_WRONG_MESSAGE = "Something went wrong please try again.";
export const SOMETHING_WENT_WRONG_CODE = "handlerError";

export const SUCCESS_STATUS = "success";
export const SUCCESS_CODE = "200";
