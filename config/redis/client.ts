/**
 * redis client for news-api
 */

import * as Redis from "ioredis";

import {
    REDIS_HOST,
    REDIS_PORT,
} from "../constants";

const redis = new Redis(REDIS_HOST, REDIS_PORT);

export default redis;
