/**
 * start point for news-api
 */

/// <reference path="./typings.d.ts" />

"use strict";

import * as Hapi from "hapi";

import {
    PORT,
} from "./config/constants";

import plugins, { enableCors } from "./plugins";

import routes from "./routes";

const server = new Hapi.Server();
server.connection({ port: PORT });

plugins(server);

server.ext("onPreResponse", enableCors);
server.route(routes);

server.start((err) => {

    if (err) {
        throw err;
    }
    console.log(`Server running at: ${server.info.uri}`);
});

export default server;
