#!/bin/bash

usage="$(basename $0) [-h] -- program to make, tag, and push docker
where:
    -h | --help  optional: show this help text
    -n | --name  required: name for the docker
    -t | --tag   required: tag for the docker
    -p | --production  optional: for production build\n"


HELP="NO"

POSITIONAL=()
while [[ $# -gt 0 ]]
do
key=$1

case $key in
    -n|--name)
    NAME=$2
    shift
    shift
    ;;
    -t|--tag)
    TAG=$2
    shift
    shift
    ;;
    -h|--help)
    HELP="YES"
    shift
    shift
    ;;
    -p|--prod)
    STAGING=""
    shift
    shift
    ;;
esac
done

if [[ $HELP == "YES" ]]
then
  printf "$usage"
  exit
fi

if [ -z $NAME ]
then
    echo "Please insert name. For help use -h or --help"
    exit
fi

if [ -z $TAG ]
then
    echo "Please insert tag. For help use -h or --help"
    exit
fi

docker build -t tprabodh/$NAME .
docker tag tprabodh/$NAME tprabodh/$NAME:$TAG
docker push tprabodh/$NAME:$TAG

echo "pushed $NAME:$TAG"