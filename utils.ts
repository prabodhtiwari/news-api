/**
 * utils for news-api
 */

"use strict";

import * as rp from "request-promise";

import { Model } from "typings";

import {
    filter,
    map,
} from "lodash";

import {
    CACHE_EXPIRE_TIME,
    NEWS_API_KEY,
    NEWS_API_URI,
    SUCCESS_CODE,
    SUCCESS_STATUS,
} from "./config/constants";
import redis from "./config/redis/client";

export const getNews = async (country: string, category: string, keyword: string): Promise<Model.Response> => {
    let fromCache = true;
    const redisKey = getRedisKey(country, category);
    let news = await getNewsFromCache(redisKey);
    if (news.length === 0) {
        fromCache = false;
        news = await getNewsFromApi(country, category);
        redis.set(redisKey, JSON.stringify(news));
        redis.expire(redisKey, CACHE_EXPIRE_TIME);
    }

    return {
        articles: map(keywordsFilter(keyword, news), (datum) => getResponse(datum, keyword, country, category)),
        code: SUCCESS_CODE,
        fromCache,
        status: SUCCESS_STATUS,
    };
};

const getNewsFromApi = (country: string, category: string): Promise<Model.Article[]> => {
    const options = {
        json: true,
        qs: {
            apiKey: NEWS_API_KEY,
            category,
            country,
        },
        uri: NEWS_API_URI,
    };

    return rp(options)
        .then((data) => data.articles)
        .catch((err) => {
            return [];
        });
};

const keywordsFilter = (keyword: string, rawArticles: Model.Article[]): Model.Article[] => {
    if (keyword) {
        return filter(rawArticles, (rawArticle: Model.Article) => {
            const title = rawArticle.title ? rawArticle.title.toLowerCase() : "";
            const description = rawArticle.description ? rawArticle.description.toLowerCase() : "";
            return title.indexOf(keyword) > -1 || description.indexOf(keyword) > -1;
        });
    } else {
        return rawArticles;
    }
};

const getResponse = (article: Model.Article, keyword: string, country: string, category: string):
    Model.ResponseArticle => {
    return {
        category,
        country,
        description: article.description,
        keyword,
        title: article.title,
        url: article.url,
    } as Model.ResponseArticle;
};

const getNewsFromCache = (key: string): Promise<Model.Article[]> => {
    return redis.get(key)
        .then(JSON.parse)
        .then((data) => data ? data : [])
        .catch((err) => []);
};

export const getRedisKey = (country: string, category: string): string => {
    return `news::${country}::${category}`;
};
