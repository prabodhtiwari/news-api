/**
 * news-api typings file
 */

declare module 'typings' {
    namespace Model {
        interface NewsApiRes {
            status: string
            totalResults: number
            articles: Article[]
        }

        interface Article {
            author: string
            description: string
            publishedAt: Date
            source: Source
            title: string
            url: string
            urlToImage: string
        }

        interface Source {
            id: string
            name: string
        }

        interface Response {
            code: string
            status: string
            articles: ResponseArticle[]
            fromCache: boolean
        }

        interface ResponseArticle {
            country: string
            category: string
            keyword: string
            title: string
            description: string
            url: string
        }
    }
}