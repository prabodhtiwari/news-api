News-api
-----------

**A service to find relevant news for a particular Category and Country.**

Written in TypeScript

## Setup instructions

### For development

Development prerequisites:

    Node
    Npm
    Redis

Set following environment variabls:
    REDIS_HOST
    REDIS_PORT

**Install dependencies**

`npm install`

**Run unit tests**

`npm run compile`
`npm run test`

**Start server**

`npm run compile`
`npm run dev`

### For Production

Production prerequisites:

    Docker
    Redis

**Build docker**
`./builddocker.sh -n news-api -t 1.6.0 -p`

**Run docker**

Set following environment variabls in `env.list` file:
    REDIS_HOST
    REDIS_PORT

for help run `./builddocker.sh -h`

move `env.list` and `.rundocker.sh` to server
run `./rundocker.sh -n news-api -i 3000 -o 3000 -e env.list -t 1.6.0 -p`

docker login credentials
    username : tprabodh
    password : Prabodh@123