
/**
 * routes for news-api
 */

"use strict";

import {
    RouteConfiguration,
} from "hapi";

import * as Joi from "joi";
import handler from "./handler";

const routes: RouteConfiguration[] = [
    {
        config: {
            validate: {
                query: {
                    category: Joi.string().allow("").description("category to get news from"),
                    country: Joi.string().allow("").description("country to get news from"),
                    keyword: Joi.string().allow("").description("keyword for news filter"),
                },
            },
        },
        handler: handler.news,
        method: "GET",
        path: "/news",
    },
];

export default routes;
