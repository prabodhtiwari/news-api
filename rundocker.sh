#!/bin/bash

usage="$(basename $0) [-h] -- program to run docker
where:
    -h | --help             optional: show this help text
    -n | --name             required: name of the docker
    -t | --tag              required: tag for the docker
    -i |--inport            required: for docker port
    -o |--outport           required: for machine port
    -e |--envfile           required: address of env file
    -s |--src               optional: address of src default is /tmp \n"


HELP="NO"
SRC="\tmp"


POSITIONAL=()
while [[ $# -gt 0 ]]
do
key=$1
case $key in
    -n|--name)
    NAME=$2
    shift
    shift
    ;;
    -t|--tag)
    TAG=$2
    shift
    shift
    ;;
    -i|--inport)
    INPORT=$2
    shift
    shift
    ;;
    -o|--outport)
    OUTPORT=$2
    shift
    shift
    ;;
    -e|--envfile)
    ENVFILE=$2
    shift
    shift
    ;;
    -s|--src)
    SRC=$2
    shift
    shift
    ;;
    -h|--help)
    HELP="YES"
    shift
    shift
    ;;
    -p|--prod)
    STAGING=""
    shift
    shift
    ;;
esac
done

if [[ $HELP == "YES" ]]
then
  printf "$usage"
  exit
fi

if [ -z $NAME ]
then
    echo "Please insert name. For help use -h or --help"
    exit
fi

if [ -z $TAG ]
then
    echo "Please insert tag. For help use -h or --help"
    exit
fi

if [ -z $ENVFILE ]
then
    echo "Please insert env file address. For help use -h or --help"
    exit
fi

if [ -z $INPORT ]
then
    echo "Please insert docker port. For help use -h or --help"
    exit
fi

if [ -z $OUTPORT ]
then
    echo "Please insert machine port. For help use -h or --help"
    exit
fi

if [ -z $SRC ]
then
    echo "Please insert src address. For help use -h or --help"
    exit
fi

echo "docker stop $NAME || true && docker rm $NAME || true && docker run --name $NAME  --env-file $ENVFILE -p $OUTPORT:$INPORT -d tprabodh/$NAME:$TAG"

docker stop $NAME || true && docker rm $NAME || true && docker run --name $NAME  --env-file $ENVFILE -p $OUTPORT:$INPORT -d tprabodh/$NAME:$TAG