/*!
        File Description: Defines non-volatile middleware for the application.
*/

"use strict";

import * as Good from "good";
import * as HapiSwagger from "hapi-swagger";
import * as Inert from "inert";
import * as Vision from "vision";

import {
    ReplyNoContinue,
    ReplyWithContinue,
    Request,
    Server,
} from "hapi";

const Ping = {
    register(server: Server, options, next) {
        server.route({
            config: {
                description: "Ping server",
                notes: "Health check route, should reply with pong",
                tags: ["misc"],
            },

            handler: (request: Request, reply: ReplyNoContinue) => reply("pong"),
            method: "GET",
            path: "/ping",
        });
        next();
    },
};

Ping.register["attributes"] = {
    name: "Ping",
    version: "1.0.0",
};

const plugins = [
    {
        options: {
            reporters: {
                myConsoleReporter: [
                    {
                        args: [{ log: "*", response: "*" }],
                        module: "good-squeeze",
                        name: "Squeeze",
                    },
                    {
                        module: "good-console",
                    },
                    "stdout",
                ],
                myFileReporter: [
                    {
                        args: [{ ops: "*" }],
                        module: "good-squeeze",
                        name: "Squeeze",
                    },
                    {
                        module: "good-squeeze",
                        name: "SafeJson",
                    },
                    {

                        args: [`${process.env.LOGS_FOLDER}/gamezop-api`],
                        module: "good-file",
                    },
                ],
            },
        },
        register: Good,
    },
    { register: Ping },
];

const developmentPlugins = [
    { register: Vision },
    { register: Inert },
    {
        options: {
            info: {
                description: "Gamezop's general api for login",
                title: "Family api",
                version: "1.0.0",
            },
        },
        register: HapiSwagger,
    },
];

const errorHandler = (err) => {
    if (err) {
        throw err;
    }
};

const attachPlugins = (server: Server) => {
    if (process.env.NODE_ENV !== "production") {
        server.register(developmentPlugins, errorHandler);
    }
    server.register(plugins as any, errorHandler);
};

export const enableCors = (request: Request, reply: ReplyWithContinue) => {
    if (!request.headers["origin"]) {
        return reply.continue();
    }
    const response = request.response.isBoom ? request.response["output"] : request.response;
    response.headers["access-control-allow-origin"] = request.headers["origin"];
    response.headers["access-control-allow-credentials"] = "true";
    if (request.method !== "options") {
        return reply.continue();
    }
    response.statusCode = 200;
    response.headers["access-control-expose-headers"] = "content-type, content-length, etag";
    response.headers["access-control-max-age"] = `${60 * 10}`;
    if (request.headers["access-control-request-headers"]) {
        response.headers["access-control-allow-headers"] = request.headers["access-control-request-headers"];
    }
    if (request.headers["access-control-request-method"]) {
        response.headers["access-control-allow-methods"] = request.headers["access-control-request-method"];
    }
    reply.continue();
};

export default attachPlugins;
